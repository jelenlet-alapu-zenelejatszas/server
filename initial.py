from pymongo import MongoClient
from dotenv import dotenv_values

dotenv_config = dotenv_values(".env")
uri = f"mongodb://{dotenv_config['ADMIN_USERNAME']}:{dotenv_config['ADMIN_PASSWORD']}@localhost:27017/"

client = MongoClient(uri)
mydb = client["smarthome"]

mycol = mydb["locations"]

mycol.insert_one({
    "_id": "br1",
    "name": "Main Bedroom",
    "address": "192.168.100.31"
})
mycol.insert_one({
    "_id": "lr1",
    "name": "Main Livingroom", 
    "address": "192.168.100.41"
})

mycol = mydb["users"]

mycol.insert_one({
    "name": "botlabovics",
    "location": "br1"
})