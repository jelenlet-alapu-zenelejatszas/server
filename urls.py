from flask_restful import Resource
from src.locations import GetLocations
from src.devicelocation import DeviceLocation
from src.userlocation import UserLocation
from src.devices import Devices
from src.users import Users

class Index(Resource):
    def get(self):
        return {"Hello": "World"}

urls = [
    ('/', Index),
    ('/locations', GetLocations),
    ('/userlocation/<string:user>', UserLocation),
    ('/users', Users),
    ('/devices', Devices),
    ('/devicelocation/<string:device>', DeviceLocation),
]

def setControllerUrls(api):
    for (url, cls) in urls:
            api.add_resource(cls, url)