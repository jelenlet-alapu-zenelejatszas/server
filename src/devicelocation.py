from flask_restful import Resource
from pymongo import MongoClient
from settings import uri


class DeviceLocation(Resource):
    def get(self, device):
        client = MongoClient(uri)
        coll = client.smarthome.locations
        result = coll.find_one({"_id": device})
        client.close()
        if result:
            result['_id'] = str(result['_id'])
            return result
        else:
            return ("Not found", 404)