from datetime import datetime
from flask import request
from flask_restful import Resource
from pymongo import MongoClient
from requests import post
from settings import uri


def log_event(data):
    data['timestamp'] = datetime.now()
    client = MongoClient(uri)
    log_db = client.smarthome.log

    log_db.insert_one(data)
    
    client.close()


def send_post(address, msg_type, meta):
    """Send a post request with the specified parameters

    Args:
        address (string): IP address to send to
        msg_type (string): Message type
        meta (dict): Meta information

    Returns:
        Response: Response from the server
    """
    r = post(f"http://{address}", json={"type": msg_type,"meta": meta})
    return r
    

class Users(Resource):
    def get(self):
        resp = {}
        client = MongoClient(uri)
        users = client.smarthome.users.find()
        resp["users"] = [ item for item in users]
        client.close()
        return resp
    
    def post(self):
        args = request.get_json()
        if not args:
            return {"error": "Provide JSON to update user location"}

        """
        Expected JSON format:
        {
            "dev_id": "dev_id",
            "user": "user_card_id",
            "location": {
                "from": "loc1",
                "to": "loc2"
            }
        }
        """
        device_id = args.get('dev_id', None)
        if device_id is None:
             return {"error": "Provide device ID"}
        
        user_card_id = args.get('user', None)
        if user_card_id is None:
             return {"error": "Provide user card ID"}
        
        location = args.get('location', None)
        if location is None:
             return {"error": "Provide device location"}

        client = MongoClient(uri)
        users_db = client.smarthome.users
        locations_db = client.smarthome.locations
        devices_db = client.smarthome.devices

        from_location = locations_db.find_one({'_id': location['from']})
        to_location = locations_db.find_one({'_id': location['to']})
        if (not from_location) or (not to_location):
            return {"error": "Location ID error"}
        
        user = users_db.find_one({"card_id": user_card_id})
        if not user:
            return {"error": "User ID error"}
        if user['current_location'] == location['to']:
            return {"error": "User is already there"}
        
        device = devices_db.find_one({"_id": user['active_device']})
        if not device:
                    return {"error": "Device ID error"}

        device_location = locations_db.find_one({"_id": device['location']})
        users_db.update_one({"_id": user['_id']}, {"$set": {'current_location': to_location['_id']}})
        client.close()
        
        log_event({
            'type': "user_location_change",
            'meta': {
                'user': user['_id'],
                'from': from_location['_id'],
                'to': to_location['_id'],
                'device_id': device_id
            } 
        })

        # Measure RSSI for handover
        resp = send_post(device_location['address'], "measure_rssi", {"address": device['mac']})
        handover = resp.json()
        print(handover)

        # Stop playback
        send_post(from_location['address'], "stop_recv", \
                      meta={
                           "address": device['multicast'], 
                           "from_location": from_location['_id'], 
                           "device_location": device_location['_id']
                        }
                    )

        if handover["IsHandover"]:
            # BT Disconnect
            send_post(from_location['address'], "disconnect_bt", {"address": device['mac']})
            # BT Connect
            send_post(to_location['address'], "connect_bt", {"mac_address": device['mac'], "multicast_address": device['multicast']})

        # Start playback
        send_post(to_location['address'], "start_recv", meta={
                "multicast_address": device['multicast'],
                "mac_address": device['mac'],
                "to_location": to_location['_id'],
                "device_location": device_location['_id']
            })

        return {"success": "success"}