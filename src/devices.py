from flask import request
from flask_restful import Resource
from pymongo import MongoClient
from settings import uri


def device_connection(args):
    req_meta = args.get('meta', None)
    if req_meta is None:
        return {"error": "No meta supplied"}
    
    client = MongoClient(uri)
    devices_db = client.smarthome.devices
    """
    Expected JSON format:
    {
        "type": "device_connection",
        "meta": {
            "mac": "18:87:40:DD:9A:51",
            "location": 'br1'
        }
    }
    """
    # Find device by MAC
    device = devices_db.find_one({'mac': req_meta['mac']})
    if not device:
        return {"error": "Device MAC not found"}
    
    # Update device location
    resp = devices_db.update_one({"_id": device['_id']}, {"$set": {'location': req_meta['location']}})
    client.close()

    if resp.acknowledged:
        return device['multicast']
    else:
        return "Failed"


def device_disconnection(args):
    req_meta = args.get('meta', None)
    if req_meta is None:
        return {"error": "No meta supplied"}
    
    client = MongoClient(uri)
    devices_db = client.smarthome.devices
    """
    Expected JSON format:
    {
        "type": "device_disconnection",
        "meta": {
            "mac": "18:87:40:DD:9A:51",
            "location": 'br1'
        }
    }
    """

    # Find device by MAC
    device = devices_db.find_one({'mac': req_meta['mac']})
    if not device:
        return {"error": "Device MAC not found"}
    
    # Update device location
    resp = devices_db.update_one({"_id": device['_id']}, {"$set": {'location': None}})
    client.close()
    
    if resp.acknowledged:
        return device['multicast']
    else:
        return "Failed"


dispatcher = {
    "device_connection": device_connection,
    "device_disconnection": device_disconnection,
}


class Devices(Resource):
    def get(self):
        resp = {}
        client = MongoClient(uri)
        devices = client.smarthome.devices.find()
        resp["devices"] = [ item for item in devices]
        client.close()
        return resp
    
    def post(self):
        args = request.get_json()
        if not args:
            return {"error": "Provide JSON to update device location"}
        
        if 'type' not in args.keys():
            return {"error": "No type supplied"}
        
        resp = dispatcher.get(args['type'], {"error": "Request type error"})(args['meta'])
        return resp
                    