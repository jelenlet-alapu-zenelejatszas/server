from flask_restful import Resource
from pymongo import MongoClient
from settings import uri


class GetLocations(Resource):
    def get(self):
        resp = {}
        client = MongoClient(uri)
        result = client.smarthome.locations.find()
        resp["locations"] = [ item for item in result]
        client.close()
        return resp