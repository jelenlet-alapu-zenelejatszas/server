from flask_restful import Resource
from pymongo import MongoClient
from settings import uri


class UserLocation(Resource):
    def get(self, user):
        if not user:
            return ("Not found", 404)

        client = MongoClient(uri)
        coll = client.smarthome.users
        user_entry = coll.find_one({"name": user})
        
        if not user_entry:
            return ("Not found", 404)

        coll = client.smarthome.locations
        address = coll.find_one({"_id": user_entry["current_location"]})
        client.close()
        return {'user': user, 'address': address}