from os import getenv
from dotenv import dotenv_values


dotenv_config = dotenv_values(".env")
if getenv('MONGODB_URL'):
    uri = getenv('MONGODB_URL')
else:
    uri = f"mongodb://{dotenv_config['ADMIN_USERNAME']}:{dotenv_config['ADMIN_PASSWORD']}@localhost:27017/"



