from flask import Flask
from flask_restful import Api
from urls import setControllerUrls


app = Flask(__name__)
api = Api(app)

setControllerUrls(api)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')